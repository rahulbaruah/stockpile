@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
        <div class="box-body">
          <div class="col-md-6 col-xs-12">
              <div class="row">
            <form class="form-horizontal" action="{{ url('report/sales-report') }}" method="get">
              
              <div class="col-md-5">
                  <label for="exampleInputEmail1">{{ trans('message.report.from') }}</label>
                  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control" id="from" type="text" name="from" value="<?= isset($from) ? $from : '' ?>">
                  </div>
              </div>

              <div class="col-md-5">
                  <label for="exampleInputEmail1">{{ trans('message.report.to') }}</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control" id="to" type="text" name="to" value="<?= isset($to) ? $to : '' ?>">
                  </div>
              </div>

              <div class="col-md-2">
                <label for="btn">&nbsp;</label>
                <button type="submit" name="btn" class="btn btn-primary btn-flat">{{ trans('message.extra_text.filter') }}</button>
              </div>
            </form>
          </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <br>
            <div class="btn-group pull-right">
              <a href="{{URL::to('/')}}/report/sales-report-csv" title="CSV" class="btn btn-default btn-flat" id="csv">{{ trans('message.extra_text.csv') }}</a>
              <a href="{{URL::to('/')}}/report/sales-report-pdf" title="PDF" class="btn btn-default btn-flat" id="pdf">{{ trans('message.extra_text.pdf') }}</a>
            </div>

          </div>

        </div>
        <br>
      </div><!--Top Box End-->

          <?php
            $qty = 0;
            $sales = 0;
            $cost = 0;
            $order = 0;
            $profit = 0;
          ?>
            @foreach ($itemList as $item)
            <?php
            $order += $item->total_order;
            $qty += $item->qty;
            $sales += $item->sale;
            $cost += $item->purchase;
            $profit = $sales-$cost;
            ?>
            @endforeach

      <div class="box">
        <div class="box-body">
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{number_format($order,2,'.',',')}}</h3>
              <span class="text-info">{{ trans('message.report.no_of_orders') }}</span>
          </div>
          <div class="col-md-2 col-xs-6 border-right text-center">
              <h3 class="bold">{{ number_format($qty,2,'.',',')}}</h3>
              <span class="text-info">{{ trans('message.report.sales_volume') }}</span>
          </div>
          <div class="col-md-3 col-xs-6 border-right text-center">
              <h3 class="bold">{{ Session::get('currency_symbol').number_format($sales ,2,'.',',')}}</h3>
              <span class="text-info">{{ trans('message.report.sales_value') }} </span>
          </div>
          <div class="col-md-3 col-xs-6 border-right text-center">
              <h3 class="bold">{{Session::get('currency_symbol').number_format($cost,2,'.',',')}}</h3>
              <span class="text-info">{{ trans('message.report.cost') }}</span>
          </div>
          <div class="col-md-2 col-xs-6 text-center">
              <h3 class="bold">
                @if($profit<0)
                -{{Session::get('currency_symbol').number_format(abs($profit),2,'.',',')}}
                @else
               {{Session::get('currency_symbol').number_format(abs($profit),2,'.',',')}}
                @endif
              </h3>
              @if($profit<0)
              <span class="text-info">{{ trans('message.report.profit') }}</span>
              @else
              <span class="text-info">{{ trans('message.report.profit') }}</span>
              @endif
          </div> 
        </div>
      </div><!--Top Box End-->

      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="salesList" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="text-center">{{ trans('message.report.date') }}</th>
                  <th class="text-center">{{ trans('message.report.no_of_orders') }}</th>
                  <th class="text-center">{{ trans('message.report.sales_volume') }}</th>
                  <th class="text-center">{{ trans('message.report.sales_value') }}({{Session::get('currency_symbol')}})</th>
                  <th class="text-center">{{ trans('message.report.cost') }}({{Session::get('currency_symbol')}})</th>
                  <th class="text-center">{{ trans('message.report.profit') }}({{Session::get('currency_symbol')}})</th>
                  <th class="text-center">{{ trans('message.report.profit_margin') }}(%)</th>    
                </tr>
                </thead>
                <tbody>
                <?php
                  $qty = 0;
                  $sales = 0;
                  $cost = 0;
                  $order = 0;
                ?>
                @foreach ($itemList as $item)

                <tr>
                  <td class="text-center"><a href="{{URL::to('/')}}/report/sales-report-by-date/{{ strtotime($item->ord_date) }}">{{ formatDate($item->ord_date) }}</a></td>
                  <td class="text-center">{{ $item->total_order }}</td>
                  <td class="text-center">{{ $item->qty }}</td>
                  <td class="text-center">{{ number_format(($item->sale),2,'.',',') }}</td>
                  <td class="text-center">{{ number_format(($item->purchase),2,'.',',') }}</td>
                  <td class="text-center">{{ number_format(($item->sale-$item->purchase),2,'.',',') }}</td>
                  <td class="text-center">{{number_format((($item->sale-$item->purchase))*100/$item->purchase,2)}}</td>
                </tr>
               @endforeach
               
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>

@include('layouts.includes.message_boxes')
@endsection
@section('js')
<script type="text/javascript">
  $(function () {
  $(".select2").select2({});
    
    $('#from').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: '{{Session::get('date_format_type')}}'
    });

    $('#to').datepicker({
        autoclose: true,
        todayHighlight: true,
        
        format: '{{Session::get('date_format_type')}}'
    });



    $("#salesList").DataTable({
      "order": [],

      "columnDefs": [ {
        "targets": 3,
        "orderable": true
        } ],

        "language": '{{Session::get('dflt_lang')}}',
        "pageLength": '{{Session::get('row_per_page')}}'
    });
    

   $('#pdf').on('click', function(event){
      event.preventDefault();
      var to = $('#to').val();
      var from = $('#from').val();
      window.location = SITE_URL+"/report/sales-report-pdf?to="+to+"&from="+from;
    });

   $('#csv').on('click', function(event){
      event.preventDefault();
      var to = $('#to').val();
      var from = $('#from').val();
      window.location = SITE_URL+"/report/sales-report-csv?to="+to+"&from="+from;
    });

  });

    </script>
@endsection