<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <a href="http://xcraft.co" target="_blank">Xcraft Online Pvt. Ltd.</a> <?php echo date('Y') ?> <a href="#">{{ Session::get('name') }}</a>.</strong> All rights
    reserved.
  </footer>